import os

capacidad=18000
peso_max_bulto=500
contador_bultos=0
mas_pesado=0
mas_liviano=0
peso_final=0
ingreso_pesos=0

def precio_equipaje(kg):
    if kg >= 0 and kg <=25:
        precio=0
        return precio
    if kg >= 26 and kg <=300:
        precio=kg*1500
        return precio
    if kg >= 301 and kg <=500:
        precio=kg*2500
        return precio

def pesado(pesom):
    global mas_pesado
    if pesom >= mas_pesado:
        mas_pesado=pesom
        return mas_pesado
    return mas_pesado 

def liviano(pesol):
    global mas_liviano
    if pesol <= mas_liviano:
        mas_liviano=pesol
        return mas_liviano
    return mas_liviano

def control_equipajes(peso):
    global contador_bultos
    global capacidad
    global peso_max_bulto
    global contador_bultos
    global mas_pesado
    global mas_liviano
    global peso_final
    global ingreso_pesos
    if peso_final >= capacidad:
        print "\n------>Se ha alcanzado la capacidad maxima de carga.<-----"
        mostrar()
    if contador_bultos==0:
        mas_liviano=peso
    if (peso <= peso_max_bulto):
        contador_bultos=contador_bultos + 1
        ingreso_pesos=ingreso_pesos + precio_equipaje(peso)
        pesado(peso)
        liviano(peso)
        peso_final=peso_final+peso
        ingreso_pesos=ingreso_pesos + precio_equipaje(peso)
    else:
        print("\n--->El peso del equipaje no puede exceder los 500 Kg.<----")

def mostrar():
    print "\nHa ingresado un total de ",contador_bultos, " bultos."
    print "El bulto mas pesado pesa ",mas_pesado," Kg."
    print "El bulto mas liviano pesa ",mas_liviano," Kg."
    print "El peso promedio de los bultos es de ",(peso_final/contador_bultos)," Kg."
    print "El ingreso en pesos es de $",ingreso_pesos
    print "El ingreso en dolares es de $",(ingreso_pesos/3000)
    return False

def menu():

    print("Control de recepcion de equipajes\n")

while True:
    equipaje=input("\nIngresar peso del equipaje o pulsar 0 (cero) para mostrar >> ")
    if equipaje==0:
        mostrar()
        break
    control_equipajes(equipaje)
     

menu()





